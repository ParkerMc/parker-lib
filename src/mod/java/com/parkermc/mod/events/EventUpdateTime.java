package com.parkermc.mod.events;

import com.parkermc.api.amnesia.AmnesiaEvent;
import com.parkermc.api.amnesia.IAmnesiaEvents;
import com.parkermc.mod.data.DataAmnesia;

import net.minecraftforge.common.DimensionManager;

@AmnesiaEvent
public class EventUpdateTime implements IAmnesiaEvents{
	
	@Override
	public void random() {
	}

	@Override
	public void normal() {
	}

	@Override
	public void updatePost() {
		if(!DimensionManager.getWorld(0).isRemote) {
			DataAmnesia.instance.lastReset = DataAmnesia.instance.world.getTotalWorldTime();
			DataAmnesia.instance.save();
		}
	}

	@Override
	public void setApi(boolean use) {		
	}

	@Override
	public void updatePostClient() {
	}
}
