package com.parkermc.mod.events;

import com.parkermc.api.amnesia.AmnesiaEventHandler;
import com.parkermc.mod.ModConfig;
import com.parkermc.mod.data.DataAmnesia;

import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class EventHandlerWorld {
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event) {
		if(!event.getWorld().isRemote) {
			DataAmnesia.instance = new DataAmnesia(event.getWorld());
			DataAmnesia.instance.load();
		}
	}
	
	@SubscribeEvent
	public void onTick(TickEvent.WorldTickEvent event) {
		if(ModConfig.expire > 0&&(!event.world.isRemote)&&event.world.getTotalWorldTime() > ModConfig.expire+DataAmnesia.instance.lastReset) {
			AmnesiaEventHandler.random();
		}
	}
}
