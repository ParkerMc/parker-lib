package com.parkermc.mod;

import org.apache.logging.log4j.Level;

import net.minecraftforge.fml.common.FMLLog;

public class ModLog {
	public static void error(String format, Object... data) {
		FMLLog.log(Level.ERROR, format, data);
	}
}
