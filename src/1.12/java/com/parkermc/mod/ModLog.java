package com.parkermc.mod;

import net.minecraftforge.fml.common.FMLLog;

public class ModLog {
	public static void error(String format, Object... data) {
		FMLLog.log.error(format, data);
	}
}
