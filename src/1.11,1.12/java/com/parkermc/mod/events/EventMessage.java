package com.parkermc.mod.events;

import com.parkermc.api.amnesia.AmnesiaEvent;
import com.parkermc.api.amnesia.IAmnesiaEvents;
import com.parkermc.mod.ModConfig;

import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

@AmnesiaEvent
public class EventMessage implements IAmnesiaEvents{
	
	@Override
	public void random() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ModConfig.randomMsg.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendMessage(new TextComponentString(ModConfig.randomMsg));
		}
	}

	@Override
	public void normal() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ModConfig.normMsg.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendMessage(new TextComponentString(ModConfig.normMsg));
		}
	}

	@Override
	public void updatePost() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ModConfig.postMsg.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendMessage(new TextComponentString(ModConfig.postMsg));
		}
	}

	@Override
	public void setApi(boolean use) {		
	}

	@Override
	public void updatePostClient() {
	}
}
