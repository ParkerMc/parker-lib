package com.parkermc.api.amnesia;

public interface IAmnesiaEvents{
	public void random() ;
	public void normal();
	public void updatePost();
	public void updatePostClient();
	public void setApi(boolean use);
}